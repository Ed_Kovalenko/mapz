﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public abstract class Animal
    {
        public abstract int Paws { get; set; }
        private string example = "Hi, I am an animal!";

        public void SaySomething()
        {
            Console.WriteLine(example);
        }

        public abstract void AddPaws(ref Animal animal);
    }
}
