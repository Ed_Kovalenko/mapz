﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    internal static class BoxingExample
    {
        public static void ShowExample(object o1, object o2)
        {
            Console.WriteLine(o1.ToString() + " " + o2.ToString());
            Console.WriteLine("Unboxing");
            var h1 = (Human)o1;
            var h2 = (Human)o2;
            h1.SaySomething();
            h2.SaySomething();
        }
    }
}
