﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public class Human : Mammal
    {
        private static int _count;

        public int Count { get => _count; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        protected int height = 175;
        protected int weight = 80;

        private int test = 228;

        public Human(string name, string surname, int age)
            : base(4)
        {
            Age = age;
            Name = name;
            Surname = surname;
            _count++;
        }
        static Human()
        {
            _count = 0;
        }

        public static implicit operator Human(int x)
        {
            return new Human(x.ToString(), x.ToString(), x);
        }

        public static explicit operator int(Human human)
        {
            return human.Count;
        }

        public void Deconstruct(out string personName, out string personSurname, out int personAge)
        {
            personName = Name;
            personAge = Age;
            personSurname = Surname;
        }

        public override string ToString()
        {
            return $"Hi, my name is {this.Name} {this.Surname}!";
        }

        public class CardID
        {
            public Guid CardNumber { get; set; }
            public DateTime ExpireDate { get; private set; }

            public CardID()
            {
                ExpireDate = DateTime.Now.AddYears(2);
                CardNumber = Guid.NewGuid();
            }
        }

    }
}
