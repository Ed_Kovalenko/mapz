﻿using Lab1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public class Mammal : Animal, IMovable
    {
        private int _paws;
        public override int Paws {
            get
            {
                return _paws;
            }
            set 
            { 
                _paws = value;
            }
        }

        public void Move()
        {
            Console.WriteLine("I am running!");
        }

        public Mammal(int paws)
        {
            this.Paws = paws;
        }

        public override string? ToString()
        {
            return $"I am a mammal and I have {Paws} paws.";
        }

        public override void AddPaws(ref Animal animal)
        {
            animal.Paws += this.Paws;
        }
    }
}
