﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public class Simple
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int ID { get; set; }
        public void Foo() => Console.WriteLine("Hello");
        public void Bar() => Console.WriteLine("World");
        public Simple(string name, string value, int id)
        {
            Name = name;
            Value = value;
            ID = id;
        }
    }
}
