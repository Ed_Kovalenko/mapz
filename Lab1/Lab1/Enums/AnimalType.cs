﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Enums
{
    internal enum AnimalType
    {
        Carnivore = 1,
        Herbivore = 2,
        Omnivore = 3
    }
}
