﻿using Lab1.Entities;
using Lab1.Interfaces;

Console.WriteLine("Init");

#region
//Human h1 = new Human("Eduard", "Kovalenko", 18);
//Human h2 = new Human("Taras", "Ivanov", 20);
//BoxingExample.ShowExample(h1, h2);
#endregion

#region
//EnumDemo enumDemo = new EnumDemo(Lab1.Enums.AnimalType.Carnivore, Lab1.Enums.AnimalType.Omnivore);
//enumDemo.Demonstrate();
#endregion

#region
//Human h = new Human("Test", "Test", 25);
//Human.CardID hc = new Human.CardID();
#endregion

#region
//Human h1 = new Human("Eduard", "Kovalenko", 18);
//(string name, string surname, int age) = h1;
//Console.WriteLine($"{name} {surname} {age}");
//Mammal m1 = new Mammal(5);
//Animal m2 = new Mammal(2);
//m1.AddPaws(ref m2);
//Console.WriteLine(m2);
#endregion

