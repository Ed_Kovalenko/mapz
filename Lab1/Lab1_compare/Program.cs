﻿using Lab1.Entities;
using Lab1_structs.Entities;
using System.Diagnostics;

long size = 1000000;
Stopwatch stopWatch = new Stopwatch();

List<Human> lst1 = new List<Human>();
List<Human_struct> lst2 = new List<Human_struct>();
List<Simple> lst3 = new List<Simple>();
List<Simple_struct> lst4 = new List<Simple_struct>();

List<object> ilst = new List<object>();
List<object> dlst = new List<object>();
List<object> flst = new List<object>();

stopWatch.Start();

for (int i = 0; i < size; i++)
{
    lst2.Add(new Human_struct($"{i}", $"{i}", i));
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

for (int i = 0; i < size; i++)
{
    lst1.Add(new Human($"{i}", $"{i}", i));
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

for (int i = 0; i < size; i++)
{
    lst3.Add(new Simple($"{i}", $"{i}", i));
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

for (int i = 0; i < size; i++)
{
    lst4.Add(new Simple_struct($"{i}", $"{i}", i));
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

//Boxing/Unboxing
stopWatch.Restart();

//int
for (int i = 0; i < size; i++)
{
    object obj = (object)i;
    ilst.Add(obj);
    int n = (int)obj;
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

//double
for (int i = 0; i < size; i++)
{
    double d = i;
    object obj = (object)d;
    dlst.Add(obj);
    double n = (double)obj;
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

//float
for (int i = 0; i < size; i++)
{
    float d = i;
    object obj = (object)d;
    flst.Add(obj);
    double n = (float)obj;
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);