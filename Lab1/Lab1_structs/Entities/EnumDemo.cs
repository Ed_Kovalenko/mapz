﻿using Lab1.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    internal struct EnumDemo
    {
        public AnimalType FirstType { get; set; }
        public AnimalType SecondType { get; set; }

        public EnumDemo(AnimalType t1, AnimalType t2)
        {
            FirstType = t1;
            SecondType = t2;
        }

        public void Demonstrate()
        {
            Console.WriteLine($"Operator & {FirstType & SecondType}");
            Console.WriteLine($"Operator | {FirstType | SecondType}");
            Console.WriteLine($"Operator ^ {FirstType ^ SecondType}");
        }
    }
}
