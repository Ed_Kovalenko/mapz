﻿using Lab1.Enums;
using Lab1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public struct Human_struct : IMovable
    {
        private static int _count;

        public int Paws { get; set; }

        public int Count { get => _count; }
        public AnimalType Type { get; private set; }
        public string Name { get; set; }

        internal void SaySomething()
        {
            Console.WriteLine("I can speak!");
        }

        public string Surname { get; set; }
        public int Age { get; set; }

        public void AddPaws(ref Human_struct animal)
        {
            animal.Paws += this.Paws;
        }

        public void Move()
        {
            Console.WriteLine("I am running!");
        }

        public Human_struct(string name, string surname, int age)
        {
            Type = AnimalType.Omnivore;
            Age = age;
            Name = name;
            Surname = surname;
            _count++;
            Paws = 4;
        }

        static Human_struct()
        {
            _count = 0;
        }

        public static implicit operator Human_struct(int x)
        {
            return new Human_struct(x.ToString(), x.ToString(), x);
        }

        public static explicit operator int(Human_struct human)
        {
            return human.Count;
        }

        public void Deconstruct(out string personName, out string personSurname, out int personAge)
        {
            personName = Name;
            personAge = Age;
            personSurname = Surname;
        }

        public override string ToString()
        {
            return $"Hi, my name is {this.Name} {this.Surname}!";
        }

        public class CardID
        {
            public Guid CardNumber { get; set; }
            public DateTime ExpireDate { get; private set; }

            public CardID()
            {
                ExpireDate = DateTime.Now.AddYears(2);
                CardNumber = Guid.NewGuid();
            }
        }

    }
}
