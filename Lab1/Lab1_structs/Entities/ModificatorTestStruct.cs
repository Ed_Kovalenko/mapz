﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    struct ModificatorTest
    {
        int a = 1;
        int b = 2;
        private int c = 3;
        int d = 4;
        public int e = 5;

        public ModificatorTest(int a, int b, int c, int d, int e)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }

        public ModificatorTest()
        {
        }
    }
}
