﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1_structs.Entities
{
    public struct Simple_struct
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int ID { get; set; }
        public void Foo() => Console.WriteLine("Hello");
        public void Bar() => Console.WriteLine("World");
        public Simple_struct(string name, string value, int id)
        {
            Name = name;
            Value = value;
            ID = id;
        }
    }
}
