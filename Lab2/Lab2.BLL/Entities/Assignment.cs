﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public class Assignment : IComparer<Assignment>, IComparable<Assignment>
    {
        public int ID { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int ProjectID { get; set; }

        public Assignment(int id, string name, string description, DateTime startDate, DateTime finishDate, int projectID)
        {
            ID = id;
            Name = name;
            Description = description;
            StartDate = startDate;
            FinishDate = finishDate;
            ProjectID = projectID;
        }

        public override string? ToString()
        {
            return $"{ID} {Name} {ProjectID}";
        }

        public int Compare(Assignment? x, Assignment? y)
        {
            if (x.FinishDate > y.FinishDate)
            {
                return 1;
            }
            else if (x.FinishDate < y.FinishDate)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public int CompareTo(Assignment? other)
        {
            return this.Compare(this, other);
        }
    }
}
