﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    /// <summary>
    /// Converts lists of elements in corresponding dictionaries
    /// </summary>
    public static class DictMaker
    {
        /// <summary>
        /// Takes list of all projects and make the dictionary with key - id
        /// and value - correspodning project
        /// </summary>
        /// <param name="lst"></param>
        /// <returns>Dictionary of projects</returns>
        public static Dictionary<int, Project> ProjectsDictionary(List<Project> lst)
        {
            var res = lst.ToDictionary(x => x.ID, x => x);
            return res;
        }

        /// <summary>
        /// Takes list of all tasks and make the dictionary with key - project id
        /// and value as list of tasks
        /// </summary>
        /// <param name="lst"></param>
        /// <returns>Dictionary with tasks grouped by project id</returns>
        public static Dictionary<int, List<Assignment>> TasksDictionary(List<Assignment> lst)
        {
            var ls = lst
                .GroupBy(p => p.ProjectID)
                .OrderBy(g => g.Count())
                .ToDictionary(g => g.Key, g => g.OrderBy(p => p.Name)
                .ToList());
            
            return ls;
        }

    }
}
