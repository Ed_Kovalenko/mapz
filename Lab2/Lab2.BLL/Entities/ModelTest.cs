﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public class ModelTest
    {
        public List<Project> Projects
        {
            get
            {
                return new List<Project>()
                {
                    new Project("1Project1", "desc1", 1),
                    new Project("3Project2", "desc2", 2),
                    new Project("1Project3", "desc3", 3),
                    new Project("9Project4", "desc4", 4),
                    new Project("5Project5", "desc5", 5),
                    new Project("7Project6", "desc6", 6),
                    new Project("9Project7", "desc7", 7)

                };
            }
        }

        public List<Assignment> Assignments
        {
            get
            {
                return new List<Assignment>()
                {
                    new Assignment(1, "9Task1", "desktask1", DateTime.Now, DateTime.Now.AddDays(20), 1),
                    new Assignment(2, "8Task2", "desktask2", DateTime.Now, DateTime.Now.AddDays(30), 1),
                    new Assignment(3, "tTask3", "desktask3", DateTime.Now, DateTime.Now.AddDays(40), 2),
                    new Assignment(4, "5Task4", "desktask4", DateTime.Now, DateTime.Now.AddDays(10), 3),
                    new Assignment(5, "4Task5", "desktask5", DateTime.Now, DateTime.Now.AddDays(25), 3),
                    new Assignment(6, "tTask6", "desktask6", DateTime.Now, DateTime.Now.AddDays(20), 3),
                    new Assignment(7, "8Task7", "desktask7", DateTime.Now, DateTime.Now.AddDays(80), 4),
                    new Assignment(8, "9Task8", "desktask8", DateTime.Now, DateTime.Now.AddDays(90), 5),
                    new Assignment(9, "0Task9", "desktask9", DateTime.Now, DateTime.Now.AddDays(50), 6),
                    new Assignment(9, "0Task9", "desktask9", DateTime.Now, DateTime.Now.AddDays(50), 6),
                    new Assignment(9, "0Task9", "desktask9", DateTime.Now, DateTime.Now.AddDays(50), 6),
                    new Assignment(9, "tTask9", "desktask9", DateTime.Now, DateTime.Now.AddDays(50), 6)
                };
            }
        }

    }
}
