﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public static class OtherStructures
    {
        public static Queue<string> StringQueue(List<Assignment> lst, string str)
        {
            var queue = new Queue<string>();
            var res = lst
                .OrderBy(p => p.Name)
                .Select(l => l.Name)
                .Where(p => p.StartsWith(str));
            
            foreach (var item in res)
            {
                queue.Enqueue(item);
            }

            return queue;
        }

        public static IEnumerable<Tuple<char, int>> CountOfProjectsByFirstLetter(List<Project> lst)
        {
            var res = lst
                .Select(g => new
                {
                    Name = g.Name.First()
                })
                .GroupBy(g => g.Name)
                .Select(g => new Tuple<char, int>(g.Key, g.Count()));

            return res;
        }
    }
}
