﻿using Lab2.BLL.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Tests
{
    [TestFixture]
    public class DictionaryTests
    {
        private ModelTest _moq;

        [SetUp]
        public void SetUp()
        {
            _moq = new ModelTest();
        }

        [Test]
        public void DictMaker_ShouldReturnCollection()
        {
            var result = DictMaker.ProjectsDictionary(_moq.Projects);
            Assert.That(result, Is.Not.Empty);
        }

        [Theory]
        [TestCase(999)]
        [TestCase(888)]
        [TestCase(777)]
        public void DictMaker_UnexistingIdShouldNotReturn(int id)
        {
            var result = DictMaker.ProjectsDictionary(_moq.Projects);
            Assert.Throws<KeyNotFoundException>(() => { _ = result[id]; });
        }
    }
}
