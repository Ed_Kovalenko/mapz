﻿using Lab2.BLL.Entities;
using NUnit.Framework;

namespace Lab2.Tests
{
    [TestFixture]
    public class OtherStructuresTests
    {
        private ModelTest _moq;

        [SetUp]
        public void SetUp()
        {
            _moq = new ModelTest();
        }

        [Test]
        public void Test_Example()
        {
            var result = false;
            Assert.IsFalse(result, "1 should not be prime");
        }

        [Theory]
        [TestCase("a")]
        [TestCase("q")]
        [TestCase("qa")]
        public void OtherStructures_ShouldReturnEmptyCollection(string str)
        {
            var result = OtherStructures.StringQueue(_moq.Assignments, str);
            Assert.That(result, Is.Empty);
        }

        [Theory]
        [TestCase("t")]
        [TestCase("9")]
        [TestCase("0T")]
        public void OtherStructures_ShouldReturnCollection(string str)
        {
            var result = OtherStructures.StringQueue(_moq.Assignments, str);
            Assert.That(result, Is.Not.Empty);
        }

    }
}