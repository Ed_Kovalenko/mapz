using Lab2.BLL.Entities;

namespace Lab2
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var moq = new ModelTest();

            // Extension methods demo.
            MessageBox.Show(moq.Assignments.GetStringFormat());
            MessageBox.Show(moq.Projects.GetStringFormat());

            foreach (var item in moq.Projects)
            {
                var str = string.Format("Id: {0}, Name: {1}", item.ID, item.Name) + Environment.NewLine;
                textBox1.AppendText(str);
            }

            // Sorting by IComparer
            var sortedTasks = moq.Assignments.OrderBy(p => p);

            foreach (var item in sortedTasks)
            {
                var str = string.Format("Id: {0}, Name: {1}, ProjectID: {2}", item.ID, item.Name, item.ProjectID) + Environment.NewLine;
                textBox2.AppendText(str);
            }

            var d1 = DictMaker.ProjectsDictionary(moq.Projects);
            var d2 = DictMaker.TasksDictionary(moq.Assignments);

            var q1 = OtherStructures.StringQueue(moq.Assignments, "t");

            textBox3.AppendText(String.Join(Environment.NewLine, d1.Select(l => l.Key.ToString() + " " + l.Value.ToString()).ToArray()));
            textBox3.AppendText(Environment.NewLine);
            textBox3.AppendText(Environment.NewLine);
            textBox3.AppendText(String.Join(Environment.NewLine, d2.Select(l => l.Key.ToString() + " " + String.Join(", ", l.Value))));

            textBox4.AppendText(String.Join(Environment.NewLine, q1));
        }
    }
}