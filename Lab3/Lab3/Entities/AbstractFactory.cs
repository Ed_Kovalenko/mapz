﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public abstract class AbstractFactory
    {
        public abstract AbstractPhone CreatePhone();
        public abstract AbstractComputer CreateComputer();
    }
}
