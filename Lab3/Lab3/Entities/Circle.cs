﻿using Lab3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class Circle : IFigure
    {
        private int _radius;
        public Circle(int r)
        {
            _radius = r;
        }

        public IFigure Clone()
        {
            return new Circle(this._radius);
        }

        public void GetInfo()
        {
            Console.WriteLine("Circle with radius {0}", _radius);
        }
    }
}
