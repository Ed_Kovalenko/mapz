﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class FirstItemFactory : AbstractFactory
    {
        public override AbstractComputer CreateComputer()
        {
            return new Tablet();
        }

        public override AbstractPhone CreatePhone()
        {
            return new LandLinePhone();
        }
    }
}
