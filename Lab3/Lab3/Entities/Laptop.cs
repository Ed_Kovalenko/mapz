﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class Laptop : AbstractComputer
    {
        public override string? ToString()
        {
            return "I am a laptop";
        }
    }
}
