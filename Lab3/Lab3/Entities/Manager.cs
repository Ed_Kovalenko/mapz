﻿using Lab3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class Manager : IManager
    {
        private static Manager _instance;

        private List<ICompany> _companyList;

        public void AddCompany(ICompany company)
        {
            _companyList.Add(company);
        }

        private Manager()
        {

        }

        public static Manager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Manager();
            }

            return _instance;
        }
    }
}
