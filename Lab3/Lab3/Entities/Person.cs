﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class Person
    {
        private AbstractPhone _phone;
        private AbstractComputer _computer;

        public Person(AbstractFactory factory)
        {
            _phone = factory.CreatePhone();
            _computer = factory.CreateComputer();
        }

        public override string? ToString()
        {
            return $"{_phone}\n{_computer}";
        }
    }
}
