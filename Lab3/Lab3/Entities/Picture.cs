﻿using Lab3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class Picture
    {
        private IFigure _figure;
        public void Redraw(IFigure figure)
        {
            _figure = figure.Clone();
        }

        public Picture(IFigure figure)
        {
            _figure = figure.Clone();
        }

        public void ShowPicture()
        {
            _figure.GetInfo();
        }
    }
}
