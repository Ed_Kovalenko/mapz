﻿using Lab3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class Rectangle : IFigure
    {
        private int _width;
        private int _height;

        public Rectangle(int w, int h)
        {
            _width = w;
            _height = h;
        }

        public IFigure Clone()
        {
            return new Rectangle(this._width, this._height);
        }

        public void GetInfo()
        {
            Console.WriteLine("Rectangle height {0} и width {1}", _height, _width);
        }
    }
}
