﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class SecondItemFactory : AbstractFactory
    {
        public override AbstractComputer CreateComputer()
        {
            return new Laptop();
        }

        public override AbstractPhone CreatePhone()
        {
            return new MobilePhone();
        }
    }
}
