﻿using Lab3.Entities;
using Lab3.Interfaces;

#region singleton_demo
Console.WriteLine("Singleton demo");
IManager manager = Manager.GetInstance();
#endregion

#region abstract_factory_demo
AbstractFactory firstFactory = new FirstItemFactory();
AbstractFactory secondFactory = new SecondItemFactory();
Person p1 = new Person(secondFactory);
Person p2 = new Person(firstFactory);
Console.WriteLine(p1);
Console.WriteLine(p2);
#endregion

#region prototype_demo
IFigure circle = new Circle(5);
IFigure rectangle = new Rectangle(2, 3);
Picture picture = new Picture(rectangle);
picture.ShowPicture();
picture.Redraw(rectangle);
picture.ShowPicture();
#endregion
