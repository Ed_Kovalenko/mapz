﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class BulgerianPizza : Pizza
    {
        public BulgerianPizza() 
            : base("Bulgerian pizza")
        { 

        }

        public override int GetCost()
        {
            return 8;
        }
    }
}
