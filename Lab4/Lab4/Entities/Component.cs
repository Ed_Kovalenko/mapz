﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal abstract class Component
    {
        protected string Name { get; set; }

        protected Component(string name)
        {
            Name = name;
        }

        public virtual void Print()
        {
            Console.WriteLine(Name);
        }
    }
}
