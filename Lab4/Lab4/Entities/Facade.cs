﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class Facade
    {
        private Food _food;
        private Order _order;
        private Tips _tips;

        public Facade(Food food, Order order, Tips tips)
        {
            _food = food;
            _order = order;
            _tips = tips;
        }

        public void Start()
        {
            _food.CallWaiter();
            _order.MakeOrder();
            _food.Eat();
        }

        public void Stop()
        {
            _food.CallWaiter();
            _tips.Pay();
        }
    }
}
