﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class Food
    {
        public void Eat()
        {
            Console.WriteLine("I am eating now!");
        }

        public void CallWaiter()
        {
            Console.WriteLine("Waiter! Come here please!");
        }
    }
}
