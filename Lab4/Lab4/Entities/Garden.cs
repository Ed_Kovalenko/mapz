﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class Garden : Component
    {
        private List<Component> _components;

        public Garden(string name) 
            : base(name)
        {
            _components = new List<Component>();
        }

        public void Add(Component component)
        {
            _components.Add(component);
        }

        public void Remove(Component component)
        {
            _components.Remove(component);
        }

        public override void Print()
        {
            Console.WriteLine("Node " + Name);
            Console.WriteLine("Subnodes:");
            foreach (var item in _components)
            {
                item.Print();
            }
        }
    }
}
