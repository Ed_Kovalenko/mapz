﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class Person
    {
        public void FillEnergy(Facade facade)
        {
            facade.Start();
            facade.Stop();
        }
    }
}
