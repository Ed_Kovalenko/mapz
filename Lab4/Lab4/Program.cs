﻿using Lab4.Entities;

#region facade_demo
Console.WriteLine("Facade demo");
Person person = new Person();
Facade facade = new Facade(new Food(), new Order(), new Tips());
person.FillEnergy(facade);
#endregion

#region decorator_demo
Console.WriteLine("Decorator demo");
Pizza pizza = new ItalianPizza();
Console.WriteLine(pizza.Name);
pizza = new CheesePizza(pizza);
Console.WriteLine(pizza.Name);
pizza = new TomatoPizza(pizza);
Console.WriteLine(pizza.Name);
#endregion

#region composite_demo
Console.WriteLine("Composite demo");
Garden territory = new Garden("Territory");

Garden garden1 = new Garden("First garden");
garden1.Add(new Tree("g1c1"));
garden1.Add(new Tree("g1c2"));

Garden garden2 = new Garden("Second garden garden");
garden2.Add(new Tree("g2c1"));

Garden garden3 = new Garden("Third garden");
garden3.Add(new Tree("g3c1"));
garden3.Add(new Tree("g3c2"));
garden3.Add(new Tree("g3c3"));

territory.Add(garden1);
territory.Add(garden2);
territory.Add(garden3);

territory.Print();
#endregion