﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class Advertisment
    {
        // Based methods..
        protected virtual void FindAuditory()
        {
            Console.WriteLine("Default find ad method");
        }

        protected virtual void PublicAdvertisment()
        {
            Console.WriteLine("Defaultfind public ad method");
        }

        protected virtual void PayForAdvertisment()
        {
            Console.WriteLine("Default pay for ad method");
        }

        // Template method.
        public void StartAdvertise()
        {
            FindAuditory();
            PublicAdvertisment();
            PayForAdvertisment();
        }
    }
}
