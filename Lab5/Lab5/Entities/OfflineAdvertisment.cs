﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class OfflineAdvertisment : Advertisment
    {
        protected override void FindAuditory()
        {
            Console.WriteLine("Post ads on walls");
        }

        protected override void PayForAdvertisment()
        {
            Console.WriteLine("Pay real money");
        }

        protected override void PublicAdvertisment()
        {
            Console.WriteLine("Public performs in room");
        }
    }
}
