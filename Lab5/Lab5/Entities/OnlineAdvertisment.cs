﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class OnlineAdvertisment : Advertisment
    {
        protected override void FindAuditory()
        {
            Console.WriteLine("Post ad in the internet");
        }

        protected override void PayForAdvertisment()
        {
            Console.WriteLine("Pay using paypal");
        }

        protected override void PublicAdvertisment()
        {
            Console.WriteLine("Post ad in social webs");
        }
    }
}
