﻿using Lab5.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class Reception : Mediator, IObserver
    {
        private string _msg;
        public Colleague Person { get; set; }
        public Colleague Worker { get; set; }

        public override void Send(Colleague colleague)
        {
            if (Person == colleague)
                Person.Notify("Worker made the work!");
            else
                Worker.Notify("Person is asking you!");
        }

        public void Update(string message)
        {
            _msg = message;
            Console.WriteLine("Reception says: " + _msg);
        }
    }
}
