﻿using Lab5.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class Staff : Colleague, IObserver
    {
        private string _msg;

        public Staff(Mediator mediator) 
            : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("Message to staff: " + message);
        }

        public override void Send()
        {
            Console.WriteLine("I start the work!");
            mediator.Send(this);
        }

        public void Update(string message)
        {
            _msg = message;
            Console.WriteLine("Staff says: " + _msg);
        }
    }
}
