﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class Visitor : Colleague
    {
        public Visitor(Mediator mediator) 
            : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("Message to visitor: " + message);
        }

        public override void Send()
        {
            Console.WriteLine("Fix this thing, please!");
            mediator.Send(this);
        }
    }
}
