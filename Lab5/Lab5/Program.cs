﻿using Lab5.Entities;
using Lab5.Interfaces;

#region mediator_demo
Console.WriteLine("Mediator demo");
Reception reception = new Reception();
Colleague staff = new Staff(reception);
Colleague visitor = new Visitor(reception);
reception.Worker = staff;
reception.Person = visitor;
visitor.Send();
staff.Send();
#endregion

#region observer_demo
Console.WriteLine("Observer demo");
IObservable director = new Director();
director.AddObserver((Staff)staff);
director.AddObserver(reception);
director.NotifyObservers("Made the hotel brilliant!");
#endregion

#region template_method_demo
Console.WriteLine("Template method demo");
Advertisment advertisment = new OnlineAdvertisment();
advertisment.StartAdvertise();
advertisment = new OfflineAdvertisment();
advertisment.StartAdvertise();
#endregion